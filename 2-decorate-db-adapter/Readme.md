![picture](uml2.png)

Декорируем класс адаптера:

```php
$dbAdapter = new DbAdapter(new PostgresConnection('dbname=test'));

$loggedAdapter = new DbAdatperLogged($adapter, new Logger());
$statAdapter = new DbAdapterStatDecorator($loggedAdapter, new ConcreteStatisticStorage());
```
