<?php

interface DbConnectionInterface
{
    /**
     * Выполнить запрос и вернуть кол-во затронутых строк.
     * Реализовать собственную обработку ошибок выполнения запроса
     *
     * @param string $sql
     * @param array  $bindings
     * @return int
     */
    public function execute($sql, array $bindings = []): int;

    /**
     * Выполнить SQL и для каждой строки выборки вызвать указанную callback-функцию
     *     callback принимает строку ТОЛЬКО как МАССИВ
     * Реализовать собственную обработку ошибок выполнения запроса
     *
     * @param string   $sql
     * @param callable $callback
     * @param array    $bindings
     */
    public function each($sql, callable $callback, array $bindings = []);
}
