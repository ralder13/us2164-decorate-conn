<?php

class DbAdapterLogged implements DbAdapterInterface
{
    /** @var DbAdapter */
    private $originalAdapter;

    private $logger;

    /**
     * @param DbAdapter $originalAdapter
     * @param           $logger
     */
    public function __construct(DbAdapter $originalAdapter, $logger)
    {
        $this->originalAdapter = $originalAdapter;
        $this->logger = $logger;
    }

    // Execute
    // ------------------------------------------------------------------------

    /**
     * Выполнить запрос не предполагающий возврат значений
     *
     * @param string $query
     * @param array  $bindings
     * @return bool|int
     */
    public function execute($query, $bindings = [])
    {
        $this->logger->log($query);
        return $this->originalAdapter->execute($query, $bindings);
    }


    // Select
    // ------------------------------------------------------------------------

    /**
     * Вернуть список ВСЕХ строк
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectAll($query, $bindings = []): array
    {
        $this->logger->log($query);
        return $this->originalAdapter->selectAll($query, $bindings);
    }

    /**
     * Вернуть КОЛОНКУ ввиде массива
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectColumn($query, $bindings = []): array
    {
        $this->logger->log($query);
        return $this->originalAdapter->selectColumn($query, $bindings);
    }

    /**
     * Key-Value
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectKeyValue($query, $bindings = []): array
    {
        $this->logger->log($query);
        return $this->originalAdapter->selectKeyValue($query, $bindings);
    }

    /**
     * Вернуть ОДНУ строку
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectRow($query, $bindings = []): array
    {
        $this->logger->log($query);
        return $this->originalAdapter->selectRow($query, $bindings);
    }

    /**
     * Вернуть значение ОДНОЙ ЯЧЕЙКИ
     *
     * @param string $query
     * @param array  $bindings
     * @return string|false - если ничего не найдено
     */
    public function selectValue($query, $bindings = [])
    {
        $this->logger->log($query);
        return $this->originalAdapter->selectValue($query, $bindings);
    }
}
