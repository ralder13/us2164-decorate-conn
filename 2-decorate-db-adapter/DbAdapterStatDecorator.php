<?php

class DbAdapterStatDecorator implements DbAdapterInterface
{
    /** @var DbAdapter */
    private $originalAdapter;

    /** @var StatStorageInterface */
    private $statStore;

    /**
     * @param DbAdapter            $originalAdapter
     * @param StatStorageInterface $statStore
     */
    public function __construct(DbAdapter $originalAdapter, StatStorageInterface $statStore)
    {
        $this->originalAdapter = $originalAdapter;
        $this->statStore = $statStore;
    }

    // Execute
    // ------------------------------------------------------------------------

    /**
     * Выполнить запрос не предполагающий возврат значений
     *
     * @param string $query
     * @param array  $bindings
     * @return bool|int
     */
    public function execute($query, $bindings = [])
    {
        $start = time();
        $result = $this->originalAdapter->execute($query, $bindings);
        $duration = time() - $start;

        $this->statStore->saveStatistic([
            'sql' => $query,
            'duration' => $duration,
        ]);

        return $result;
    }


    // Select
    // ------------------------------------------------------------------------

    /**
     * Вернуть список ВСЕХ строк
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectAll($query, $bindings = []): array
    {
        $start = time();
        $result = $this->originalAdapter->selectAll($query, $bindings);
        $duration = time() - $start;

        $this->statStore->saveStatistic([
            'sql' => $query,
            'duration' => $duration,
        ]);

        return $result;
    }

    /**
     * Вернуть КОЛОНКУ ввиде массива
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectColumn($query, $bindings = []): array
    {
        $start = time();
        $result = $this->originalAdapter->selectColumn($query, $bindings);
        $duration = time() - $start;

        $this->statStore->saveStatistic([
            'sql' => $query,
            'duration' => $duration,
        ]);

        return $result;
    }

    /**
     * Key-Value
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectKeyValue($query, $bindings = []): array
    {
        $start = time();
        $result = $this->originalAdapter->selectKeyValue($query, $bindings);
        $duration = time() - $start;

        $this->statStore->saveStatistic([
            'sql' => $query,
            'duration' => $duration,
        ]);

        return $result;
    }

    /**
     * Вернуть ОДНУ строку
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectRow($query, $bindings = []): array
    {
        $start = time();
        $result = $this->originalAdapter->selectRow($query, $bindings);
        $duration = time() - $start;

        $this->statStore->saveStatistic([
            'sql' => $query,
            'duration' => $duration,
        ]);

        return $result;
    }

    /**
     * Вернуть значение ОДНОЙ ЯЧЕЙКИ
     *
     * @param string $query
     * @param array  $bindings
     * @return string|false - если ничего не найдено
     */
    public function selectValue($query, $bindings = [])
    {
        $start = time();
        $result = $this->originalAdapter->selectValue($query, $bindings);
        $duration = time() - $start;

        $this->statStore->saveStatistic([
            'sql' => $query,
            'duration' => $duration,
        ]);

        return $result;
    }
}
