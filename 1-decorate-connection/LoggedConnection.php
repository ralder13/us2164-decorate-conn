<?php

class LoggedConnection implements DbConnectionInterface
{
    private $logger;

    /** @var DbConnectionInterface */
    private $originalConnection;

    /**
     * @param DbConnectionInterface $origConnection
     * @param                       $logger
     */
    public function __construct(DbConnectionInterface $origConnection, $logger)
    {
        $this->originalConnection = $origConnection;
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($sql, array $bindings = []): int
    {
        $this->logger->log($sql);

        return $this->originalConnection->execute($sql, $bindings);
    }

    /**
     * {@inheritdoc}
     */
    public function each($sql, callable $callback, array $bindings = [])
    {
        $this->logger->log($sql);
        $this->originalConnection->each($sql, $callback, $bindings);
    }
}
