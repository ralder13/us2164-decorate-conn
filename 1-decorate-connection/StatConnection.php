<?php

class StatConnection implements DbConnectionInterface
{
    /** @var StatStorageInterface */
    private $statStore;

    /** @var DbConnectionInterface */
    private $originalConnection;

    /**
     * @param DbConnectionInterface $origConnection
     * @param StatStorageInterface  $store
     */
    public function __construct(DbConnectionInterface $origConnection, StatStorageInterface $store)
    {
        $this->originalConnection = $origConnection;
        $this->statStore = $store;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($sql, array $bindings = []): int
    {
        $start = time();
        $result = $this->originalConnection->execute($sql, $bindings);
        $duration = time() - $start;

        $this->statStore->saveStatistic([
            'sql' => $sql,
            'duration' => $duration,
        ]);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function each($sql, callable $callback, array $bindings = [])
    {
        $start = time();
        $this->originalConnection->each($sql, $callback, $bindings);
        $duration = time() - $start;

        $this->statStore->saveStatistic([
            'sql' => $sql,
            'duration' => $duration,
        ]);
    }
}
