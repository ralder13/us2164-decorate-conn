![picture](uml1.png)

Декорируем класс cоединения:

```php
$pgConnection = new PostgresConnection('dbname=test');

$loggedConnection = new LoggedConnection($pgConnection, new Logger());
$statConnection = new StatConnection($loggedConnection, new ConcreteStatisticStorage());

$db = new DbAdapter($statConnection);
```
