<?php

interface QueryDecoratePluginInterface
{
    public function beforeQuery(string $sql);
    public function afterQuery(string $sql);
}
