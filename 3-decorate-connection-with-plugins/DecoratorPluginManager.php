<?php

class DecoratorPluginManager
{
    /** @var QueryDecoratePluginInterface[] */
    private $plugins = [];

    /**
     * @param QueryDecoratePluginInterface $plugin
     * @return $this
     */
    public function addPlugin(QueryDecoratePluginInterface $plugin)
    {
        $this->plugins[] = $plugin;

        return $this;
    }

    public function handleBeforeQuery(string $sql)
    {
        foreach ($this->plugins as $plugin) {
            $plugin->beforeQuery($sql);
        }
    }

    public function handleAfterQuery(string $sql)
    {
        foreach ($this->plugins as $plugin) {
            $plugin->afterQuery($sql);
        }
    }
}
