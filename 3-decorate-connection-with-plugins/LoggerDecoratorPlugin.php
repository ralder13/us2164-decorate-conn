<?php

class LoggerDecoratorPlugin implements QueryDecoratePluginInterface
{
    private $logger;

    public function __construct($logger)
    {
        $this->logger = $logger;
    }

    public function beforeQuery(string $sql)
    {
        $this->logger->log($sql);
    }

    public function afterQuery(string $sql)
    {
    }
}
