<?php

class DbAdapter
{
    /** @var DbConnectionInterface */
    private $connection;

    /**
     * DbAdapter constructor.
     *
     * @param DbConnectionInterface $connection
     */
    public function __construct(DbConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return DbConnectionInterface
     */
    public function getConnection()
    {
        return $this->connection;
    }

    // Execute
    // ------------------------------------------------------------------------

    /**
     * Выполнить запрос не предполагающий возврат значений
     *
     * @param string $query
     * @param array  $bindings
     * @return bool|int
     */
    public function execute($query, $bindings = [])
    {
        return $this->getConnection()->execute($query, $bindings);
    }


    // Select
    // ------------------------------------------------------------------------

    /**
     * Вернуть список ВСЕХ строк
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectAll($query, $bindings = []): array
    {
        $result = [];

        $this->getConnection()->each((string)$query, function ($row) use (&$result) {
            $this->_checkCallbackArguments($row);
            $result[] = $row;
        }, $bindings);

        return $result;
    }

    /**
     * Вернуть КОЛОНКУ ввиде массива
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectColumn($query, $bindings = []): array
    {
        $result = [];

        $this->getConnection()->each((string)$query, function ($row) use (&$result) {
            $this->_checkCallbackArguments($row);
            $result[] = current($row);
        }, $bindings);

        return $result;
    }

    /**
     * Key-Value
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectKeyValue($query, $bindings = []): array
    {
        $result = [];

        $this->getConnection()->each((string)$query, function ($row) use (&$result) {
            $this->_checkCallbackArguments($row);
            if (count($row) != 2) {
                throw new \RuntimeException(__METHOD__ . ": Expected 2 columns, got " . var_export($row, true));
            }
            $result[current($row)] = next($row);
        }, $bindings);

        return $result;
    }

    /**
     * Вернуть ОДНУ строку
     *
     * @param string $query
     * @param array  $bindings
     * @return array
     */
    public function selectRow($query, $bindings = []): array
    {
        if ($rows = $this->selectAll($query, $bindings)) {
            foreach ($rows as $row) {
                return $row;
            }
        }

        return [];
    }

    /**
     * Вернуть значение ОДНОЙ ЯЧЕЙКИ
     *
     * @param string $query
     * @param array  $bindings
     * @return string|false - если ничего не найдено
     */
    public function selectValue($query, $bindings = [])
    {
        if ($row = $this->selectRow($query, $bindings)) {
            return current($row);
        }
        return false;
    }

    /**
     * Выкинуть исключение, если входящее значение не массив
     *
     * @param mixed $row
     */
    private function _checkCallbackArguments($row)
    {
        if (!is_array($row)) {
            throw new \InvalidArgumentException(__METHOD__ . ": Expected " . get_class($this->getConnection()) . "->each() runs callback with row as ARRAY");
        }
    }
}
