<?php

class DecoratedConnection implements DbConnectionInterface
{
    /** @var DecoratorPluginManager */
    private $pluginsManager;

    /** @var DbConnectionInterface */
    private $originalConnection;

    /**
     * @param DbConnectionInterface  $origConnection
     * @param DecoratorPluginManager $pluginsManager
     */
    public function __construct(
        DbConnectionInterface $origConnection,
        DecoratorPluginManager $pluginsManager
    ) {
        $this->originalConnection = $origConnection;
        $this->pluginsManager = $pluginsManager;
    }

    /**
     * {@inheritdoc}
     */
    public function execute($sql, array $bindings = []): int
    {
        $this->pluginsManager->handleBeforeQuery($sql);
        $result = $this->originalConnection->execute($sql, $bindings);
        $this->pluginsManager->handleAfterQuery($sql);

        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function each($sql, callable $callback, array $bindings = [])
    {
        $this->pluginsManager->handleBeforeQuery($sql);
        $this->originalConnection->each($sql, $callback, $bindings);
        $this->pluginsManager->handleAfterQuery($sql);
    }
}
