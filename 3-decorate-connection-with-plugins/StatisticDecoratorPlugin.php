<?php

class StatisticDecoratorPlugin implements QueryDecoratePluginInterface
{
    /** @var StatStorageInterface */
    private $statStorage;

    private $startTime;

    public function __construct(StatStorageInterface $statStorage)
    {
        $this->statStorage = $statStorage;
    }

    public function beforeQuery(string $sql)
    {
        $this->startTime = time();
    }

    public function afterQuery(string $sql)
    {
        $duration = time() - $this->startTime;
        $this->statStorage->saveStatistic([
            'sql' => $sql,
            'duration' => $duration,
        ]);
    }
}
