![picture](uml3.png)

Декорируем класс cоединения менеджером плагинов: 

```php
$pgConnection = new PostgresConnection('dbname=test');

$pluginManager = new DecoratorPluginManager();
$pluginManager->addPlugin(new LoggerDecoratorPlugin(new Logger()));
$pluginManager->addPlugin(new StatisticDecoratorPlugin(new ConcreteStatisticStorage()));

$decoratedConnection = new DecoratedConnection($pgConnection, $pluginManger);

$db = new DbAdapter($decoratedConnection);
```
